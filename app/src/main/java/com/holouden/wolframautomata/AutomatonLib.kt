package com.holouden.wolframautomata

import kotlin.random.Random

class AutomatonLib
{
    companion object
    {
        private var rules : HashMap<List<Boolean>, Boolean> = HashMap(8)

        private fun setRules()
        {
            rules[listOf(true, true, true)]    = Random.nextBoolean()
            rules[listOf(true, true, false)]   = Random.nextBoolean()
            rules[listOf(true, false, true)]   = Random.nextBoolean()
            rules[listOf(true, false, false)]  = Random.nextBoolean()
            rules[listOf(false, true, true)]   = Random.nextBoolean()
            rules[listOf(false, true, false)]  = Random.nextBoolean()
            rules[listOf(false, false, true)]  = Random.nextBoolean()
            rules[listOf(false, false, false)] = Random.nextBoolean()
        }

        fun getOrigGen(len: Int) : BooleanArray
        {
            setRules()

            val og = BooleanArray(len)
            og[len/2] = true
            return og
        }

        fun calcNextGen(curGen : BooleanArray) : BooleanArray
        {
            val len = curGen.size
            val ng = BooleanArray(len)

            for (i in 0 until len)
            {
                // looping corners
                val prev = if (i-1 < 0) curGen[i-1+len] else curGen[(i-1)%len]
                val cur = curGen[i]
                val next = curGen[(i+1)%len]

                ng[i] = rules[listOf(prev, cur, next)]!!
            }
            return ng
        }
    }
}