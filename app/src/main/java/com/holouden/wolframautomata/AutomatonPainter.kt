package com.holouden.wolframautomata

import android.content.Context
import android.graphics.*
import android.view.MotionEvent
import android.view.SurfaceView
import com.holouden.wolframautomata.AutomatonLib as AL


class AutomatonPainter(ctx: Context) : SurfaceView(ctx), Runnable
{
    private val cellSize = 5
    private val paint = Paint()

    private lateinit var bitm : Bitmap
    private lateinit var bitmCvs : Canvas
    private lateinit var cells : ArrayList<BooleanArray>

    private lateinit var mainCvs : Canvas

    private var scroll = false
    private var locked = false
    private var running = true

    private lateinit var paintThread : Thread

    private var holdtime : Long = 0

    override fun run()
    {
        paint.setARGB(255, 139, 195, 74) // light green
        paint.strokeWidth = cellSize.toFloat()

        while (running)
        {
            // Do I need some sleep here?
            if (!holder.surface.isValid)
                continue

            mainCvs = holder.lockCanvas()

            if (scroll)
            {
                mainCvs.drawColor(Color.BLACK)

                scrollCells() // could pass canvas to avoid keeping two of them
            }

            // drawing the bitmap with automaton
            mainCvs.drawBitmap(bitm, 0f, 0f, paint)

            holder.unlockCanvasAndPost(mainCvs)
        }
    }

    override fun onTouchEvent(e: MotionEvent): Boolean
    {
        when (e.actionMasked)
        {
            MotionEvent.ACTION_DOWN,
            MotionEvent.ACTION_POINTER_DOWN -> {
                holdtime = System.currentTimeMillis()
                return true}

            MotionEvent.ACTION_UP,
            MotionEvent.ACTION_POINTER_UP -> {
                if (System.currentTimeMillis() - holdtime > 1000)
                    scroll = true
                else if (!locked)
                    restart()
                return true }
        }
        return super.onTouchEvent(e)
    }

    fun resume()
    {
        running = true
        paintThread = Thread(this)
        paintThread.start()
    }
    fun pause()
    {
        running = false
        paintThread.join()
    }

    private fun scrollCells()
    {
        bitmCvs.drawColor(Color.BLACK)

        // TODO: make it great, Threads?
        for (y in 1 until cells.size)
        {
            cells[y - 1] = cells[y]
            if (y == cells.size - 1) cells[y] = AL.calcNextGen(cells[y])

            for (x in 0 until cells[y - 1].size)
            {
                if (cells[y - 1][x])
                    draw(x, y)
            }
        }
    }

    private fun drawCells()
    {
        Thread(Runnable{
            // cells[y] size is constant
            val rowsize = cells[0].size

            for (y in 0 until cells.size - 1) //
            {
                for (x in 0 until rowsize)
                {
                    if (cells[y][x]) draw(x, y) // drawing on the bitmap
                }
                cells[y + 1] = AL.calcNextGen(cells[y])
                Thread.sleep(5)
            }
            locked = false
        }).start()
    }
    private fun draw(x : Int, y : Int)
    {
        bitmCvs.drawPoint(x * cellSize.toFloat(), y * cellSize.toFloat(), paint)
    }


    override fun onSizeChanged(w: Int, h: Int, oldw: Int, oldh: Int)
    {
        init(w, h)
    }

    private fun restart()
    {
        // Too Many Booleans
        locked = true
        scroll = false
        pause()

        bitm.eraseColor(Color.BLACK) // remember about buffers and do not use transparent color
        while (!holder.surface.isValid) {}
        mainCvs = holder.lockCanvas()
        mainCvs.drawColor(Color.BLACK)
        holder.unlockCanvasAndPost(mainCvs)

        cells[0] = AL.getOrigGen(width/cellSize)

        resume()
        drawCells()
    }

    private fun init(w : Int, h : Int)
    {
        bitm = Bitmap.createBitmap(w, h, Bitmap.Config.ARGB_8888)
        bitmCvs = Canvas(bitm)
        cells = ArrayList(h/cellSize)

        // initializing cellular matrix
        for (i in 0 until h/cellSize)
        {
           cells.add(BooleanArray(w/cellSize))
        }
        cells[0] = AL.getOrigGen(w/cellSize)

        // animating automaton drawing
        drawCells()
    }
}