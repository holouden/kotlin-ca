package com.holouden.wolframautomata

import android.os.Bundle
import androidx.core.view.GravityCompat
import android.view.MenuItem
import androidx.drawerlayout.widget.DrawerLayout
import com.google.android.material.navigation.NavigationView
import androidx.appcompat.app.AppCompatActivity
import android.view.Menu
import android.view.View
import android.widget.Toast
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.customview.widget.ViewDragHelper
import java.lang.reflect.Field


class MainActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener {

   lateinit var painter : AutomatonPainter

    override fun onCreate(savedInstanceState: Bundle?)
    {
        super.onCreate(savedInstanceState)

        // transparent statusbar
        window.decorView.systemUiVisibility =
                (View.SYSTEM_UI_FLAG_LAYOUT_STABLE or View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN)

        setContentView(R.layout.activity_main)

        // perhaps should switch to a different type of menu
        // setting up drawer edge size
        setDrawer()

        val container : ConstraintLayout = findViewById(R.id.container)

        val navView: NavigationView = findViewById(R.id.nav_view)
        navView.setNavigationItemSelectedListener(this)

        // drawing starts here
        painter = AutomatonPainter(this)
        container.addView(painter)
    }

    override fun onBackPressed()
    {
        val drawerLayout: DrawerLayout = findViewById(R.id.drawer_layout)

        if (drawerLayout.isDrawerOpen(GravityCompat.START))
            drawerLayout.closeDrawer(GravityCompat.START)
        else
            super.onBackPressed()
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean
    {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.main, menu)
        return true
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean
    {

        Toast.makeText(this, "LMAO not yet", Toast.LENGTH_SHORT).show()

        val drawerLayout: DrawerLayout = findViewById(R.id.drawer_layout)
        drawerLayout.closeDrawer(GravityCompat.START)
//        return true
        return false
    }

    override fun onResume()
    {
        super.onResume()
        painter.resume()
    }

    override fun onPause()
    {
        super.onPause()
        painter.pause()
    }

    private fun setDrawer()
    {
        val drawer : DrawerLayout = findViewById(R.id.drawer_layout)
        val dragger : Field = drawer.javaClass.getDeclaredField("mLeftDragger")
        dragger.isAccessible = true
        val draggerHelper : ViewDragHelper = dragger.get(drawer) as ViewDragHelper
        val edgeSize : Field = draggerHelper.javaClass.getDeclaredField("mEdgeSize")
        edgeSize.isAccessible = true
        val edge = edgeSize.getInt(draggerHelper)
        edgeSize.setInt(draggerHelper, edge*5)
    }
}
