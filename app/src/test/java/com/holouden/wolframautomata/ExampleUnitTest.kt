package com.holouden.wolframautomata

import org.junit.Test

import org.junit.Assert.*

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
class ExampleUnitTest {
    @Test
    fun addition_isCorrect() {
        assertEquals(4, 2 + 2)
    }
    @Test
    fun nextGen()
    {
        var curgen = AutomatonLib.getOrigGen(5)
        curgen = AutomatonLib.calcNextGen(curgen)

        for (i in 0 until curgen.size)
        {
            when (i)
            {
                2 -> {
                    assert(curgen[i-1]);
                    assert(curgen[i]);
                    assert(curgen[i+1])}
                1,3 -> {}
                else -> assert(!curgen[i])
            }
        }
    }
}
